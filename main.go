package main

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/urfave/cli/v2"
)

const (
	patFlag       = "pat"
	projectIDFlag = "projectID"
	issueURLFlag  = "issueURL"
)

const gitlabInstance = "https://gitlab.com/api/v4"

type mergeRequest struct {
	ID           int    `json:"id"`
	IID          int    `json:"iid"`
	Title        string `json:"title"`
	TargetBranch string `json:"target_branch"`
	SourceBranch string `json:"source_branch"`
	WebURL       string `json:"web_url"`
}

func main() {
	cli := cli.App{
		Name:        "mastertomain",
		HelpName:    "mastertomain",
		Version:     "0.1.0",
		Description: "Change GitLab merge requests targeting master branch to main branch",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:     patFlag,
				Usage:    "GitLab PAT with api scope https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html#personal-access-token-scopes",
				Required: true,
			},
			&cli.Int64Flag{
				Name:     projectIDFlag,
				Usage:    "ID of the project",
				Required: true,
			},
			&cli.StringFlag{
				Name:  issueURLFlag,
				Usage: "The URL of the issue that will be used to link when leaving a note for the merge request update. If empty no note is left",
			},
		},
		Action: run,
	}

	err := cli.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func run(c *cli.Context) error {
	pat := c.String(patFlag)
	projectID := c.Int64(projectIDFlag)
	issueURL := c.String(issueURLFlag)

	mrs, err := getMergeRequests(c.Context, pat, projectID)
	if err != nil {
		return fmt.Errorf("get merge requests: %w", err)
	}

	return updateMergeRequest(c.Context, pat, projectID, mrs, issueURL)
}

func getMergeRequests(ctx context.Context, pat string, projectID int64) ([]mergeRequest, error) {
	gitlab, err := url.Parse(fmt.Sprintf("%s/projects/%d/merge_requests", gitlabInstance, projectID))
	if err != nil {
		return nil, fmt.Errorf("parsing GitLab URL: %w", err)
	}

	q := gitlab.Query()
	q.Set("state", "opened")
	q.Set("target_branch", "master")
	q.Set("page", "1")
	q.Set("per_page", "100")
	gitlab.RawQuery = q.Encode()

	client := http.Client{
		Timeout: 10 * time.Second,
	}

	req, err := http.NewRequestWithContext(ctx, http.MethodGet, gitlab.String(), nil)
	if err != nil {
		return nil, fmt.Errorf("create GET request: %w", err)
	}
	req.Header.Set("PRIVATE-TOKEN", pat)

	resp, err := client.Do(req)
	if err != nil {
		return nil, fmt.Errorf("send GET request: %w", err)
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("read request body: %w", err)
	}

	var mrs []mergeRequest
	err = json.Unmarshal(b, &mrs)
	if err != nil {
		return nil, fmt.Errorf("unmarshal json response %q: %w", b, err)
	}

	return mrs, nil
}

func updateMergeRequest(ctx context.Context, pat string, projectID int64, mrs []mergeRequest, issueURL string) error {
	client := http.Client{
		Timeout: 10 * time.Second,
	}

	for _, mr := range mrs {
		gitlab, err := url.Parse(fmt.Sprintf("%s/projects/%d/merge_requests/%d", gitlabInstance, projectID, mr.IID))
		if err != nil {
			return fmt.Errorf("parsing GitLab URL: %w", err)
		}

		q := gitlab.Query()
		q.Set("target_branch", "main")
		gitlab.RawQuery = q.Encode()

		req, err := http.NewRequestWithContext(ctx, http.MethodPut, gitlab.String(), nil)
		if err != nil {
			return fmt.Errorf("create PUT request: %w", err)
		}
		req.Header.Set("PRIVATE-TOKEN", pat)

		resp, err := client.Do(req)
		if err != nil {
			return fmt.Errorf("send PUT request: %w", err)
		}
		resp.Body.Close()

		log.Printf("Updated %s: %s", mr.WebURL, resp.Status)

		if issueURL != "" {
			err = leaveNote(ctx, pat, projectID, mr, issueURL)
			if err != nil {
				return fmt.Errorf("leave note about target branch update: %w", err)
			}
		}
	}

	return nil
}

func leaveNote(ctx context.Context, pat string, projectID int64, mr mergeRequest, issueURL string) error {
	client := http.Client{}

	gitlab, err := url.Parse(fmt.Sprintf("%s/projects/%d/merge_requests/%d/notes", gitlabInstance, projectID, mr.IID))
	if err != nil {
		return fmt.Errorf("parsing GitLab URL: %w", err)
	}

	msg := `
:wave: 

We've updated the target branch to be main since we are removing the master branch, for more information check
`

	q := gitlab.Query()
	q.Set("body", msg+issueURL)
	gitlab.RawQuery = q.Encode()

	req, err := http.NewRequestWithContext(ctx, http.MethodPost, gitlab.String(), nil)
	if err != nil {
		return fmt.Errorf("create POST request for note: %w", err)
	}
	req.Header.Set("PRIVATE-TOKEN", pat)

	resp, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("send POST request for not: %w", err)
	}
	resp.Body.Close()

	log.Printf("Left note for %s: %s", mr.WebURL, resp.Status)

	return nil
}
