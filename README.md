# mastertomain

Update the target branch of the open merge requests from `master` to `main`

## Usage

```shell
go run main.go -pat $GITLAB_API_PRIVATE_TOKEN -projectID 10644153 -issueURL https://gitlab.com/gitlab-org/gitlab-runner/-/issues/27239
```

![demo](https://i.imgur.com/PpiJjCX.png)

**Pagination:** It doesn't support pagination, it gets 100 merge request at once. You'd need to run it multiple times.